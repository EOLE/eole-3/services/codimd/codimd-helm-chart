# Changelog

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/codimd-helm-chart/compare/release/1.1.0...release/1.2.0) (2024-04-17)


### Features

* new stable version ([92e68d2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/codimd-helm-chart/commit/92e68d2005ddb8d755f6574714c4b7f8db45510d))
* new testing version ([0226a78](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/codimd-helm-chart/commit/0226a78a3a1e8e2ef560cc6c3d5a2e91ddcff04b))
* replicacount is configurable ([483a77c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/codimd-helm-chart/commit/483a77cce2f14d4df3006dad2150571a555cd064))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/codimd-helm-chart/compare/release/1.0.0...release/1.1.0) (2024-04-17)


### Features

* deploy eole3 image by default ([5093ad9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/codimd-helm-chart/commit/5093ad9e430dacf79e44fa95abdddd367989bc26))

## 1.0.0 (2023-02-23)


### Features

* **ci:** build helm package ([f88c4f3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/f88c4f35de9777d40f5789b9b7a0f8bba46d897b))
* **ci:** push helm package to chart repository ([492a329](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/492a329f4cd13f3cb3b1db0f5da5d80e846547ca))
* **ci:** update helm chart version with `semantic-release` ([aadd0ed](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/aadd0edb6500302685574e269e2c7754d9fa715e))
* **ci:** validate helm chart at `lint` stage ([fd67bc3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/fd67bc32b6adad4589b5cf2f58dabfa7f524f5e2))
* publish first stable version ([eca3233](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/eca3233c88f086e2b4afe480f13a0fa6816a1221))
* use eole harbor registry by default ([4081960](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/40819602069bbf500137485842343abd0db9b7de))


### Bug Fixes

* generate new release ([ab6bec4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/ab6bec47177cbb8c4bc019ae8b3e5547024d09ba))
* missing resources parameters in deployment ([3a87efc](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/3a87efc5595df41cee5a30ad03bddd09afc59379))
* update hpa api version removed on k8s 1.26 ([7341007](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/73410077191534b783728cb2fed2863109a5ac6c))


### Continuous Integration

* **commitlint:** enforce commit message format ([a0ec090](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/a0ec09038a442523d58a24d926a3493b08a06126))
* **release:** create release automatically with `semantic-release` ([47b5323](https://gitlab.mim-libre.fr/EOLE/eole-3/services/codimd/commit/47b53231c1e5601269505deef7f4f2e5fa52cfa4))
